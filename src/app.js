// import the express, mongodb and body-parser dependency
const express = require('express');
const bodyParser = require('body-parser');
const mongodb = require('mongodb');

// create express app
const app = express();
// define a variable for the port
const port = 3000;

// import the mongoDB dependency
const MongoClient = mongodb.MongoClient;
// define url to the database
const url = "mongodb://localhost:27017";
const dbName = 'movieDB';
const collectionName = 'movies';
let db = undefined;
let collection = undefined;

/**
 * Setup express middleware
 */
app.use(bodyParser.json());
app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    res.header("Access-Control-Allow-Methods", "GET, PUT, POST, DELETE, OPTIONS");
    next();
});


/**
 * Connect to mongo DB
 */
MongoClient.connect(url,{ useUnifiedTopology: true }, function(err, connection) {
    if (err) throw err;
    db = connection.db(dbName);
    collection = db.collection(collectionName);
});

/**
 * Checks one given movie id and returns true if it exists in the db
 */
app.get('/isFavorite/:id', (req, res) => {
    var idInt = parseInt(req.params.id);
    const query = { id: idInt};
    collection.findOne(query, function(err, obj) {
        if (err) throw err;
        console.log();
        if(obj){
            res.send(true);
        } else{
            res.send(false);
        }
    });
});

/**
 * Return all favorites
 */
app.get('/favorites', (req, res) => {
    collection.find({}).toArray(function(err, result) {
        if (err) return false;
        res.send(result);
    });
});

/**
 * Insert one favorite
 */
app.post('/favorite', (req, res) => {
    const film = req.body;
    collection.insertOne(film, function(err, result) {
        if (err) throw err;
        console.log("movie was insterted with id: " + film.id);
        res.send({result: 'movie inserted', film: film});
    });
});

/**
 * Delete favorite by id
 */
app.delete('/favorite/:id', (req, res) => {
    var idInt = parseInt(req.params.id);

    const query = { id: idInt};
    collection.findOne(query, function(err, obj) {
        if (err) throw err;
        if(obj){
            const query2 = { _id: obj._id};
            collection.deleteOne(query2, function(err, obj) {
                if (err) throw err;
                console.log("movie was deleted with id: " + idInt);
                res.send({result: 'movie deleted'});
            });
        }
    });

/**
 * Start Server
 */
app.listen(port, () => {
    console.log(`Example app listening at http://localhost:${port}`);
});
